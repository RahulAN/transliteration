# -*- coding: utf-8 -*-
from indic_transliteration import sanscript
from indic_transliteration.sanscript import SchemeMap, SCHEMES, transliterate

import sys
sys.getdefaultencoding()

print "Gujrati"
data = u'નરેન્દ્ર મોડી સચિન તેંડુલકર મધ સિંહ સ્વામી વિવેકાનંદ કોપર ખેરને સફરજનઅંબાણી'
print data.encode('utf-8')
print transliterate(data, sanscript.GUJARATI, sanscript.HK).encode('utf-8')
print ""

print "Hindi"
data = u'नरेद्र मोदी सचिन तेंडुलकर मध सिंह स्वामी विवेकानंद कोपर खैरने सफरचंद अंबानीै'
print data.encode('utf-8')
print transliterate(data, sanscript.DEVANAGARI, sanscript.HK).encode('utf-8')
print ""

print "BENGALI"
data = u'নারেদ মোদি সাকিন টেন্ডুলকার মধু সিংহ স্বামী বিবেকানন্দ কপার খায়ের আপেল আম্বানির'
print data.encode('utf-8')
print transliterate(data, sanscript.BENGALI, sanscript.HK).encode('utf-8')
print ""


print "TAMIL"
# data = u"நரேந்திர மோடி சச்சின் டெண்டுல்கர் தேன் சிங் சுவாமி விவேகானந்தர் கோப்பர் கர்ர்னே ஆப்பிள் அம்பானி"
data = u'இன்று முதல் புதிய விதி அமல் !'
print data.encode('utf-8')
print transliterate(data, sanscript.TAMIL, sanscript.HK).encode('utf-8')
print ""

print "TELUGU"
data = u"నరేంద్ర మోడి సచిన్ టెండూల్కర్ తేనె సింగ్ స్వామి వివేకానంద్ కొపెర్ ఖైర్నే ఆపిల్ అంబానీ"
print data.encode('utf-8')
print transliterate(data, sanscript.TELUGU, sanscript.HK).encode('utf-8')
print ""

print "Malyalam"
data = u'പ്രണവം കനികുങ്കങ്കര വെല്ലിക്കോത്ത് കാഞ്ഞങ്ങാട്'
print data.encode('utf-8')
print transliterate(data, sanscript.MALAYALAM, sanscript.HK).encode('utf-8')
